import xml.etree.ElementTree as ET
tree = ET.parse('address.xml')
root = tree.getroot()
# enter a value to update 
# here age increments and get updated for every iteration
for age in root.iter('age'):
	newAge = int(age.text)+1
	age.text=str(newAge)
	age.set('changed', 'yes')
	
tree.write("address.xml") 


