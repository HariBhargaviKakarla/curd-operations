from dicttoxml import dicttoxml
student = {
	"student_details": 
		{
			"name": "Kakarla Hari Bhargavi", 
			"rollnumber": "180030504",
			"phonenumber": "8637513696",
			"dob":"26/08/2000",
			"college": 
				{
				"name":"kluniversity",	
				"branch":"cse",
				"specialization":"ai",
				"cpga": "7.54"
			}
			
		}
			 
}

xml=dicttoxml(student)
xmlfile = open("std.xml", "w")
xmlfile.write(xml.decode())
xmlfile.close()
