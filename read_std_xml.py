import xmltodict

#open and read the contents of the file
with open('std.xml', 'r', encoding='utf-8') as file:
  my_xml = file.read()

#Using xmltodict module to parse and convert the XML document
my_dict = xmltodict.parse(my_xml)

print(my_dict)