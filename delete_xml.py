import xml.etree.ElementTree as ET
tree = ET.parse('address.xml')
root = tree.getroot()
# searches for all the possible mails 
for addresses in root.findall('addresses'): 
    del_data= int(addresses.find("doorno").text)
    # if the mail matches with the input mail, it deletes using remove function
    if del_data > 4:
        root.remove(addresses)    
tree.write("address.xml") 
